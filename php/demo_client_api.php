<?php

/*
 * CoSwitched provides a normal JSON API. In PHP, you can use the json
 * library to access our API.
 * 
 * This example needs at least the PHP version 5.5 in cunjunction with the
 * the json libary <http://www.php.net/manual/en/book.json.php> and
 * cURL libary <http://curl.haxx.se/docs/install.html>.
 * 
 * This example gets the list of SIM cards, shows the information of the first 
 * SIM Card and tries to activates it. To show the filter option, there is a 
 * call that gets the CDRs filtering by two dates of a specific SIM Card.
 * 
 * JSON (JavaScript Object Notation) is a lightweight data-interchange format. 
 * It is simple for humans to read as well as write. It is efficient for machines to 
 * parse and produce. It is based on a subset of the JavaScript Programming Language,
 * Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is
 * entirely language independent but applies conventions that are common to 
 * programmers of the C-family of languages, including C, C++, C#, Java, JavaScript,
 * Perl, Python, and others. These properties make JSON ideal as a
 * data-interchange language.
 * 
 * JSON is built on two structures:
 * 
 * A collection of name/value pairs. In various languages, this is realized as an object, 
 * struct, dictionary, hash table, keyed list, or associative array. 
 * An ordered list of values. In most languages, this is realized as an array, list, or sequence.
 * These are universal data structures. Virtually all modern programming 
 * languages support them in one form or another. It makes sense that a data format that is 
 * interchangeable with programming languages also be based on these structures.
 * 
 * <Source: http://json.org>
 * 
 * For more information about the CoSwitched API, see the
 * http://coswitched.com/help/api/ page.
 */


function HttpGet($url,$authentication) {
	// create a new cURL resource
	$ch = curl_init();
	$headers = array('Authorization: '.$authentication,);
	
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// grab URL and decodes with json_decode
	$result = json_decode(curl_exec($ch), true);
	
	// close cURL resource, and free up system resources
	curl_close($ch);
	
	return $result;	
}

function HttpPost($url,$authentication) {
	// create a new cURL resource
	$ch = curl_init();
	$headers = array('Authorization: '.$authentication,);
	
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	// grab URL and decodes with json_decode
	$result = json_decode(curl_exec($ch));
	
	// close cURL resource, and free up system resources
	curl_close($ch);
	
	return $result;
}

# Initialize the variables
$base_url = 'https://cp.coswitched.com/api/';
$token = ""; //Copy from Control Panel -> Settings
$authentication = "Token ".$token;

# Get the SIM Card List
echo "Request the server for the SIM Cards list\n";
$url = $base_url.'sims/';
$list_sims = HttpGet($url,$authentication);
echo var_dump($list_sims);

# Get the information from the first SIM Card of the list
echo "Request the server for the first SIM Card information\n";
$url = $base_url.'sims/'.$list_sims[0]['iccid'].'/';
$sim_info = HttpGet($url,$authentication);
echo var_dump($sim_info);

# Activate the first SIM Card of the list
echo "Request to activate the SIM Card: ".$list_sims[0]['iccid']."\n";
$url = $base_url.'sims/'.$list_sims[0]['iccid']."/activate/";
HttpPost($url,$authentication);
echo "\n";

# Filter the Call Detail Records by date
echo "Request the server for CDRs filter by date";
$url = $base_url.'sims/'.$list_sims[0]["iccid"].'/cdr/?start_date=2014-01-22%2000:00:00&end_date=2014-09-25%2023:59:59';
$list_cdrs = HttpGet($url,$authentication);
echo var_dump($list_cdrs);

?>