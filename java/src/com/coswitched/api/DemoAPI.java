package com.coswitched.api;

import java.io.IOException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;

/*
 * CoSwitched provides a normal JSON API. In Java, you can use multiple
 * libraries to access our API (e.g., Java API for JSON Processing, Jersey,
 * Jackson and Apache Commons).
 * 
 * This example uses the Java API for JSON Processing (JSR 353) and requires
 * Java 7 runtime. The example gets the list of SIM cards and activates the
 * first one.
 * 
 * The Java API for JSON Processing (JSR 353) provides portable APIs to parse, 
 * generate, transform, and query JSON using object model and streaming APIs.
 * 
 * The object model API creates a random-access, tree-like structure that 
 * represents the JSON data in memory. The tree can then be navigated and 
 * queried. This programming model is the most flexible and enables processing
 * that requires random access to the complete contents of the tree. 
 * 
 * The object model API is similar to the Document Object Model (DOM) API for 
 * XML. It is a high-level API that provides immutable object models for JSON 
 * object and array structures. These JSON structures are represented as object
 * models using the Java types JsonObject and JsonArray.
 * 
 * <Source: http://www.oracle.com/technetwork/articles/java/json-1973242.html>
 * 
 * JSR 353 was released along with the Java EE 7 platform. JsonObject and 
 * JsonReader API can be used in two different ways:
 * - Use a Java EE 7 compliant application server, such as GlassFish 4. In this
 * case, the API is built in to the runtime and will be resolved correctly for
 * you. You can use NetBeans, Eclipse or IntelliJ and if the server runtime is
 * configured properly then it just works. Alternatively, you can download the
 * Reference Implementation from 
 * http://jcp.org/aboutJava/communityprocess/final/jsr353/index.html and
 * integrate wit your application or container of your choice.
 * 
 * For more information about the CoSwitched API, see the
 * http://coswitched.com/help/api/ page.
 */
public class DemoAPI {
	
	private static String API_BASE_URL = "https://cp.coswitched.com/api/";
	private static String API_TOKEN = ""; //Copy from control panel -> Settings
		
	private String getFirstSim() throws IOException {
		// Get a new HTTP connection to perform a JSON GET request
		HttpsURLConnection uc = getJsonConnection("sims/");
		sysoutHttpsURLConnection(uc);

		// Parse the response to the JSON reader
		JsonReader rdr = Json.createReader(uc.getInputStream());

		// Read the array of JSON objects
		JsonArray sims = rdr.readArray();
		System.out.println("===\nResponse SIMs: " + sims);
		
		// Get the first object of the array
		JsonObject sim1 = sims.getJsonObject(0);
		System.out.println("===\nSIM1 ICCID: " + sim1.getJsonString("iccid"));
		
		return sim1.getJsonString("iccid").toString();
	}
	
	private void activateSim(String iccid) throws IOException {
		// Get a new HTTP connection to perform a JSON GET request
		HttpsURLConnection uc = getPostJsonConnection("sims/" + iccid + "/activate/");
		sysoutHttpsURLConnection(uc);

		// Parse the response to the JSON reader
		JsonReader rdr = null;
		if (uc.getResponseCode() == 200) {
			rdr = Json.createReader(uc.getInputStream());
		}
		else {
			rdr = Json.createReader(uc.getErrorStream());
		}
		JsonObject status = rdr.readObject();
		System.out.println("===\nJSON Response: " + status);
	}
	
	private HttpsURLConnection getJsonConnection(String uri) throws IOException {
		URL url = new URL(API_BASE_URL + uri);
		HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
		uc.setRequestProperty("Authorization", "Token " + API_TOKEN);
        uc.setRequestProperty("Accept", "application/json");
		return uc;
	}
	
	private HttpsURLConnection getPostJsonConnection(String uri) throws IOException {
		HttpsURLConnection uc = getJsonConnection(uri);
        uc.setRequestMethod("POST");
        return uc;
	}
	
	private void sysoutHttpsURLConnection(HttpsURLConnection uc) throws IOException {
		System.out.println("===\nRequest URI: " + uc.getURL());
		System.out.println("Response Code: " + uc.getResponseCode());
		System.out.println("Response Headers: " + uc.getHeaderFields());
	}
	
	public static void main(String[] args) throws IOException {
		DemoAPI demoAPI = new DemoAPI();
		String iccid = demoAPI.getFirstSim();
		demoAPI.activateSim(iccid);
	}
}
