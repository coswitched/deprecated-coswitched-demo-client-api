# README #

This repository includes demo clients of the CoSwitched API.

Each demo gets the list of the SIM cards associated with the account and activates the first one.


### How do I get set up? ###

* Download example.
* Check the main file of the example or the README file (if available).
* Configure operating system environment for the specific language (e.g., install Java virtual machine).
* Get the Token associated with your account and configure it in the example.
* Run example.


### Examples ###

* [Java](https://bitbucket.org/coswitched/coswitched-demo-client-api/src//java/?at=master)
* [Javascript (jQuery)](https://bitbucket.org/coswitched/coswitched-demo-client-api/src//javascript/?at=master)
* [PHP](https://bitbucket.org/coswitched/coswitched-demo-client-api/src//php/?at=master)
* [Python](https://bitbucket.org/coswitched/coswitched-demo-client-api/src//python/?at=master)
* [Ruby](https://bitbucket.org/coswitched/coswitched-demo-client-api/src//ruby/?at=master)


### Documentation ###
* [Help Page](https://coswitched.com/help/api/)
* [API Documentation](https://coswitched.com/api/doc/)


### Contribution guidelines ###

* We welcome and appreciate any contribution. If you have a new example (e.g., new feature or new programming language), please feel free to create a pull request or send it to [support@coswitched.com](mailto:support@coswitched.com).


### Who do I talk to? ###

* [support@coswitched.com](mailto:support@coswitched.com)
