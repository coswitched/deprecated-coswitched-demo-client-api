from StringIO import StringIO
import json
import requests

"""
CoSwitched provides a normal JSON API. In Python, you can use the json
library to access our API.

This example uses Python with the json import, requires Python 2.4
with the following additional packages:
 -> pyOpenSSL
 -> ndg-httpsclient
 -> pyasn1
<Source: https://stackoverflow.com/questions/18578439/using-requests-with-tls-doesnt-give-sni-support/18579484>

Or use Python 3.0 or above. <Source: http://docs.python-requests.org/en/latest/community/faq/>

The example gets the list of SIM cards, shows the information of the first 
SIM Card and activates it. To show the filter option, there is a 
call that gets the CDRs filtering by two dates of a specific SIM Card.
 

JSON (JavaScript Object Notation) is a lightweight data-interchange format. 
It is simple for humans to read as well as write. It is efficient for machines to 
parse and produce. It is based on a subset of the JavaScript Programming Language,
Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is
entirely language independent but applies conventions that are common to 
programmers of the C-family of languages, including C, C++, C#, Java, JavaScript,
Perl, Python, and others. These properties make JSON ideal as a
data-interchange language.

JSON is built on two structures:

A collection of name/value pairs. In various languages, this is realized as an object, 
struct, dictionary, hash table, keyed list, or associative array. 
An ordered list of values. In most languages, this is realized as an array, list, or sequence.
These are universal data structures. Virtually all modern programming 
languages support them in one form or another. It makes sense that a data format that is 
interchangeable with programming languages also be based on these structures.

<Source: http://json.org>

For more information about the CoSwitched API, see the
http://cp.coswitched.com/help/api/ page.
"""

class DemoAPI:
    def __init__(self):
        pass
     
    # Method GET to the the server 
    def request_server_get(self, authentication, url):
        # Create header needed for Authentication
        headers = {"Authorization": authentication}
        
        # Request the server
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            # Create a dictionary using JSON
            return json.load(StringIO(response.content))
        else:
            response.raise_for_status()
    
    # Method POST to the server    
    def request_server_post(self, authentication, url, data):
        # Create header needed for Authentication
        headers = {"Authorization": authentication}
        # Create string buffer     
        response = requests.post(url, headers=headers, data=data)
        if response.status_code == 200 or response.status_code == 400:
            # Create a dictionary using JSON
            return json.load(StringIO(response.content))
        else:
            response.raise_for_status()

def main(DemoAPI):
    
    print "DO NOT FORGET TO GET THE TOKEN"

    # Initialize the variables
    base_url = 'http://cp.coswitched.com/api/'
    token = "644fe538430ab93298d3602ad8e96868a53f0b90";  # Copy from Control Panel -> Settings
    authentication = "Token " + token
    
    # Get the SIM Card List
    print "Request the server for the SIM Cards list"
    url = base_url + 'sims/'
    list_sims = DemoAPI.request_server_get(authentication, url)
    print list_sims

    # Get the SIM Card List
    print "Request the server for the SIM Cards list"
    url = base_url + 'sims/'
    list_sims = DemoAPI.request_server_get(authentication, url)
    print list_sims
    
    # Get the information from the first SIM Card of the list  
    print "Request the server for the first SIM Card information"
    url = base_url + 'sims/' + list_sims[0]["iccid"]
    sim_card = DemoAPI.request_server_get(authentication, url)
    print "The SIM Card requested has the following information: "
    for keys, values in sim_card.items():
        print keys + ": " + str(values)        
        
    # Activate the first SIM Card of the list
    #print "Request to activate the SIM Card: " + list_sims[0]["iccid"]
    #url = base_url + 'sims/' + list_sims[0]["iccid"] + "/activate/"
    #print DemoAPI.request_server_post(authentication, url)['status']
    
    # Filter the Call Detail Records by date
    print "Request the server for CDRs filter by date"
    url = base_url + 'sims/' + list_sims[0]["iccid"] + '/cdr/?start_date=2014-01-22 00:00:00&end_date=2014-09-24 23:59:59'
    cdr_list = DemoAPI.request_server_get(authentication, url)
    print "These are the CDRs requested: "
    for cdr in cdr_list:
        print cdr	

    #Send SMS Call
    #print "SMS to SIM Card: " + list_sims[0]["iccid"]
    #data = {'content': ["Test from CoSwitched"],
	#		'iccid': [list_sims[0]["iccid"]]}

    #url = base_url + 'sims/send_sms/'
    #print DemoAPI.request_server_post(authentication, url, data)['status']
    



if __name__ == "__main__":
    main(DemoAPI())   


