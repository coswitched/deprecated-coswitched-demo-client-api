#!/usr/bin/env ruby

# CoSwitched provides a normal JSON and an XML API.
#
# This example uses the Ruby Rest-Client module to perform  REST requests and the JSON
# module to parse the responses. The example gets the list of SIM cards and activates
# the first one.
#
# See the Rest-Client website for information on how to install and use it:
# http://rubydoc.info/github/rest-client/rest-client
#
# See the Ruby JSON moduel page for information on how to use it:
# http://www.ruby-doc.org/stdlib-2.0.0/libdoc/json/rdoc/JSON.html
# 
# For more information about the CoSwitched API, see the
# http://coswitched.com/help/api/ page.

require 'rest_client'
require "json"

# Initialize the variables
base_url = 'https://cp.coswitched.com/api/'
token = "";  # Copy from Control Panel -> Settings
authentication = "Token " + token

# Get the SIM Card List
puts "Request the SIM Cards list"

begin
  url = base_url + 'sims/'
  puts "==\nRequest URI: " + url
  response = RestClient.get url, {:content_type => :json, :accept => :json, :Authorization => authentication}
    
  puts "Response Code: " + response.code.to_s
  puts "Response Headers: " + response.headers.to_s
  puts "==\nResponse SIMs: " + response.to_str
rescue RestClient::Exception => e
  puts "Response Code: " + e.response.code.to_s
  puts "Response Headers: " + e.response.headers.to_s
  puts "==\nResponse Content: " + e.response.to_str
  exit
end

# Parse JSON response
response = JSON.parse(response)
sim1 = response[0]['iccid']
puts "==\nSIM1 ICCID: " +sim1

# Activate the first SIM
puts "==\nActivate SIM with ICCID: " + sim1
begin
  url = base_url + 'sims/' + sim1 + '/activate/'
  puts "==\nRequest URI: " + url
  response = RestClient.post url, {}, {:content_type => :json, :accept => :json, :Authorization => authentication}
    
  puts "Response Code: " + response.code.to_s
  puts "Response Headers: " + response.headers.to_s
  puts "==\nResponse SIMs: " + response.to_str
rescue RestClient::Exception => e
  puts "Response Code: " + e.response.code.to_s
  puts "Response Headers: " + e.response.headers.to_s
  puts "==\nResponse Content: " + e.response.to_str
  exit
end